# webeos-custom-okd-project-template

Customizes the OKD4 project template for webeos use case. See https://gitlab.cern.ch/webservices/webframeworks-planning/issues/45

This includes [console customizations](https://docs.okd.io/latest/web_console/customizing-the-web-console.html)
to provide [removal of default samples](https://docs.okd.io/latest/openshift_images/configuring-samples-operator.html)
since users don't have permission to use them in the Webeos cluster.